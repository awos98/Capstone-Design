import glob
import os

from django.conf import settings
from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile
from django.http import HttpRequest
from django.test import TestCase

from common.models import User


class BaseTestCase(TestCase):
    _COMMON_PASSWORD = "1q2w3e4r!@"

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.user = cls.create_user()
        cls.request = HttpRequest()

    @classmethod
    def create_user(cls, username="user"):
        return User.objects.create_user(username=username, password=cls._COMMON_PASSWORD)

    def login(self, username="user"):
        # Load User
        self.user = User.objects.filter(username=username).get()
        self.request.user = self.user

        # Login
        self.client.login(request=self.request, username=username, password=self._COMMON_PASSWORD)

    def logout(self):
        # Logout
        self.client.logout()


class FileTestCase(BaseTestCase):
    TEST_FILE_ROOT_PATH = os.path.join(settings.BASE_DIR, "tests/mock_data/files/images")

    def get_image_file(self):
        file_binary = open(os.path.join(self.TEST_FILE_ROOT_PATH, "asd.png"), "rb").read()

        return SimpleUploadedFile("asd.png", file_binary, content_type="image/png")

    def clean_up_image_file(self):
        test_org_images = glob.glob(f"{os.path.join(settings.MEDIA_ROOT, settings.ORIGINAL_IMAGE_FILE_PATH, '*')}")
        test_cv_images = glob.glob(f"{os.path.join(settings.MEDIA_ROOT, settings.OPENCV_IMAGE_FILE_PATH, '*')}")

        print('test_org_images 제거중..')
        for org in test_org_images:
            os.remove(org)
        print('test_cv_images 제거중..')
        for cv in test_cv_images:
            os.remove(cv)
