# Generated by Django 4.0.4 on 2022-09-18 14:34

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.CharField(max_length=10000)),
                ('user', models.CharField(max_length=100)),
                ('room', models.IntegerField(default=0)),
            ],
        ),
    ]
