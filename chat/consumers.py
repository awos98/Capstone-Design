import json
import logging
import time

from channels.generic.websocket import AsyncWebsocketConsumer

from play.views import users_score
from quiz.models import Quiz
from room.models import Room
from common.models import User
from channels.db import database_sync_to_async

logger = logging.getLogger(__name__)


# 비동기적 채팅 서버
class ChatConsumer(AsyncWebsocketConsumer):
    # 연결 요청 받음
    async def connect(self):
        self.room_id = self.scope['url_route']['kwargs']['room_id']
        self.room_group_name = 'chat_%s' % self.room_id

        # 채팅방 그룹 이름 추가
        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    # 연결 요청을 끊을 때 = 페이지가 다른 페이지로 바뀔 때
    async def disconnect(self, close_code):
        # Leave room

        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        command = text_data_json['command']
        room_id = self.scope['url_route']['kwargs']['room_id']

        if command == "joined":
            host = text_data_json['host']

            user_ready_list, status = await self.host_start_ready()
            user = await self.get_user(room_id)

            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'websocket_joined',
                    'user': user,
                    'host': host,
                    'status': status,
                    'user_ready_list': user_ready_list,
                    "command": command,
                }
            )

        # Send message to room group
        if command == "chat":
            message = text_data_json['message']
            username = text_data_json['username']

            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'websocket_chat',
                    'message': message,
                    'username': username,
                    "command": command,
                }
            )

        if command == "ready":
            username = text_data_json['username']

            await self.user_status(username, command)
            user_ready_list, status = await self.host_start_ready()
            user = await self.get_user(room_id)
            host = await self.get_host()
            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'websocket_ready',
                    'username': username,
                    'user_ready_list': user_ready_list,
                    'status': status,
                    'user': user,
                    'host': host,
                    "command": command,
                }
            )

        if command == "unready":
            username = text_data_json['username']

            await self.user_status(username, command)
            user_ready_list, status = await self.host_start_ready()
            user = await self.get_user(room_id)
            host = await self.get_host()
            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'websocket_unready',
                    'username': username,
                    'user_ready_list': user_ready_list,
                    'status': status,
                    'user': user,
                    'host': host,
                    "command": command,
                }
            )

        if command == "start":
            await self.room_status_close()
            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'websocket_start',
                    "command": command,
                }
            )

        if command == "create":
            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'websocket_create',
                    "command": command,
                }
            )

        if command == "exited":
            room_id = self.scope['url_route']['kwargs']['room_id']
            exit_user = text_data_json['exit_user']

            host = await self.get_host()
            user = await self.disconnect_get_user(room_id, self.scope['user'])
            if host == exit_user:
                host = await self.host_change(room_id)

            try:
                user_ready_list, status = await self.host_start_ready()
            except:
                user_ready_list = 0
                status = False
            time.sleep(0.5)
            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'websocket_exited',
                    'user': user,
                    'host': host,
                    'status': status,
                    'user_ready_list': user_ready_list,
                    "command": 'exited',
                }
            )

    # Receive message from room group
    # receive 에서 받은걸 html 로 보내는 과정
    async def websocket_joined(self, event):
        command = event['command']

        await self.send(text_data=json.dumps({
            'user': event['user'],
            'host': event['host'],
            'status': event['status'],
            'user_ready_list': event['user_ready_list'],
            "command": command,
        }))

    async def websocket_chat(self, event):
        message = event['message']
        username = event['username']
        command = event['command']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message,
            'username': username,
            "command": command,
        }))

    async def websocket_exited(self, event):
        command = event['command']

        await self.send(text_data=json.dumps({
            'user': event['user'],
            'host': event['host'],
            'status': event['status'],
            'user_ready_list': event['user_ready_list'],
            "command": command,
        }))

    async def websocket_start(self, event):
        command = event['command']
        await self.send(text_data=json.dumps({
            "command": command,
        }))

    async def websocket_create(self, event):
        command = event['command']
        await self.send(text_data=json.dumps({
            "command": command,
        }))

    @database_sync_to_async
    def get_user(self, room_id):
        room = Room.objects.get(id=room_id)
        user = room.users.all()
        user_list = []
        for i in user:
            user_list.append(i.username)
        return list(user_list)

    @database_sync_to_async
    def disconnect_get_user(self, room_id, user):
        room = Room.objects.get(id=room_id)
        room.users.remove(user)
        room.save()
        users = room.users.all()
        user_list = []
        for i in users:
            user_list.append(i.username)
        return list(user_list)

    @database_sync_to_async
    def host_change(self, room_id):
        room = Room.objects.get(id=room_id)
        Quiz.objects.filter(room_id=room_id, user_id=room.host).delete()
        userss = room.users.all()
        if room.users.exists():
            user_list = []
            for i in userss:
                user_list.append(i.username)
            room.host = None
            new = room.users.filter().order_by("?").first()
            room.host = new
            room.save()
            host = User.objects.get(id=room.host.id)
            host.ready = 0
            host.save()
            return str(host.username)
        else:
            room.delete()
            return None

    async def websocket_ready(self, event):
        command = event['command']
        username = event['username']
        user = event['user']
        user_ready_list = event['user_ready_list']

        await self.send(text_data=json.dumps({
            'username': username,
            'user': user,
            'user_ready_list': user_ready_list,
            'status': event['status'],
            'host': event['host'],
            "command": command,
        }))

    async def websocket_unready(self, event):
        command = event['command']
        username = event['username']
        user = event['user']
        user_ready_list = event['user_ready_list']

        await self.send(text_data=json.dumps({
            'username': username,
            'user': user,
            'user_ready_list': user_ready_list,
            'status': event['status'],
            'host': event['host'],
            "command": command,
        }))

    @database_sync_to_async
    def get_host(self):
        room_id = self.scope['url_route']['kwargs']['room_id']
        room = Room.objects.get(id=room_id)
        return str(room.host)

    @database_sync_to_async
    def user_status(self, username, command):
        user = User.objects.get(username=username)

        if command == "unready":
            user.ready = 1
        else:
            user.ready = 0
        user.save()

    @database_sync_to_async
    def host_start_ready(self):
        room_id = self.scope['url_route']['kwargs']['room_id']
        room = Room.objects.get(id=room_id)
        quiz_count = Quiz.objects.filter(user_id=room.host, room_id=room)
        room_user = room.users.all()
        user_ready_list = []
        status = False

        for user in room_user:
            user_ready_list.append(user.ready)

        if (sum(user_ready_list) == room_user.count() - 1) and room.play_round == quiz_count.count():
            print("전부 레디했고 퀴즈까지 다 만들어짐")
            status = True

        return user_ready_list, status

    @database_sync_to_async
    def room_status_close(self):
        room_id = self.scope['url_route']['kwargs']['room_id']
        room = Room.objects.get(id=room_id)
        room.is_start = 1
        room.save()


#
# 비동기적 플레이 서버
class PlayConsumer(AsyncWebsocketConsumer):
    # 연결 요청 받음
    async def connect(self):
        self.room_id = self.scope['url_route']['kwargs']['room_id']
        self.room_group_name = 'play_%s' % self.room_id

        # 채팅방 그룹 이름 추가
        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    # 연결 요청을 끊을 때 = 페이지가 다른 페이지로 바뀔 때
    async def disconnect(self, close_code):
        # Leave room

        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        command = text_data_json['command']
        room_id = self.scope['url_route']['kwargs']['room_id']

        if command == "joined":
            host = text_data_json['host']

            status = await self.status(room_id)
            users_rank_list, users_score_dict = await self.get_score()
            user = await self.get_user(room_id)

            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'websocket_joined',
                    'user': user,
                    'users_rank': users_rank_list,
                    'users_score': users_score_dict,
                    'status': status,
                    'host': host,
                    "command": command,
                }
            )

        if command == "next":
            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'websocket_next',
                    "command": command,
                }
            )

        if command == "back":
            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'websocket_back',
                    "command": command,
                }
            )

    async def websocket_joined(self, event):
        await self.send(text_data=json.dumps({
            'users_rank': event['users_rank'],
            'users_score': event['users_score'],
            'user': event['user'],
            'status': event['status'],
            'host': event['host'],
            "command": event['command'],
        }))

    async def websocket_next(self, event):
        await self.send(text_data=json.dumps({
            "command": event['command'],
        }))

    async def websocket_host_exited(self, event):
        await self.send(text_data=json.dumps({
            "host": event['host'],
            "command": event['command'],
        }))

    async def websocket_back(self, event):
        await self.send(text_data=json.dumps({
            "command": event['command'],
        }))

    @database_sync_to_async
    def get_score(self):
        room_id = self.scope['url_route']['kwargs']['room_id']
        room = Room.objects.get(id=room_id)
        ranks, sorted_users_score = users_score(room)
        return ranks, sorted_users_score

    @database_sync_to_async
    def get_user(self, room_id):
        room = Room.objects.get(id=room_id)
        user = room.users.all()
        user_list = []
        for i in user:
            user_list.append(i.username)
        return list(user_list)

    @database_sync_to_async
    def status(self, room_id):
        room = Room.objects.get(id=room_id)
        room.play_user_count += 1
        room.save()
        count = room.play_user_count
        user = room.users.all()
        status = False

        if len(user) <= count:
            status = True

        return status
