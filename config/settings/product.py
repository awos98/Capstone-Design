from .base import *

DEBUG = False

ALLOWED_HOSTS = [os.environ.get('SERVER_DNS'), os.environ.get('SERVER_IP')]

MEDIA_ROOT = os.environ.get('PRODUCTION_MEDIA_ROOT')

LOGGING['root']['level'] = 'INFO'

ASGI_APPLICATION = 'config.asgi-product.application'
