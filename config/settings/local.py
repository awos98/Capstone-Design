from .base import *

DEBUG = True

ALLOWED_HOSTS = ['*']

ASGI_APPLICATION = 'config.asgi-local.application'
