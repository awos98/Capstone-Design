from .base import *

DEBUG = True

ALLOWED_HOSTS = ['*']

MEDIA_URL = 'tests/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'tests', 'media')
