import logging
import os

from random import random
from django import forms
from .models import Quiz
from django.core.files.base import ContentFile
from opencv.opencv_import import *

import cv2


logger = logging.getLogger(__name__)


class QuizForm(forms.ModelForm):
    OPENCV_FUNCTIONS = [
        canny_import,
        # findContours_import,
        mosaic_import,
        blur_import,
        lens_import,
        random_image_import
    ]

    def save(self, commit=True, *args, **kwargs):
        self._instance = super().save(commit=False)

        self._select_answer()
        self._set_original_image(kwargs['image_file'])
        self._generate_opencv_image()
        self._set_fk_user_room(kwargs['user'])
        self._instance.room_id = kwargs['room_id']

        if commit:
            self._instance.save()

        return self._instance

    def _select_answer(self):
        """선택한 정답 번호의 답안을 캐싱"""
        quiz_answer = self._instance.quiz_answer

        if quiz_answer == 1:
            self._instance.quiz_answer_name = self._instance.answer_list1
        elif quiz_answer == 2:
            self._instance.quiz_answer_name = self._instance.answer_list2
        elif quiz_answer == 3:
            self._instance.quiz_answer_name = self._instance.answer_list3
        else:
            self._instance.quiz_answer_name = self._instance.answer_list4

    def _set_original_image(self, image_file):
        self._instance.image = image_file

    def _set_fk_user_room(self, user):
        self._instance.user = user

    def _generate_opencv_image(self):
        """opencv 이미지 생성"""
        file_name = os.path.basename(self._instance.image.url)

        opencv_image = self._random_choose_opencv(self._instance.image.file.read())
        logger.debug(f'opencv_image: {opencv_image}')

        # ret(압축 결과), buf(인코딩) 이미지 압축, ContentFile를 이용하여 저장가능한 파일로 변환
        ret, buf = cv2.imencode('.jpg', opencv_image)

        self._instance.opencv_image = ContentFile(buf.tobytes(), file_name)

    @classmethod
    def _random_choose_opencv(cls, image_file: bytes):
        """해당 함수에서 이미지를 처리한다."""
        random_num = random.randrange(len(cls.OPENCV_FUNCTIONS))

        logger.debug(f"run function: {cls.OPENCV_FUNCTIONS[random_num].__name__}")

        return cls.OPENCV_FUNCTIONS[random_num](image_file)

    class Meta:
        model = Quiz
        fields = (
            'answer_list1',
            'answer_list2',
            'answer_list3',
            'answer_list4',
            'quiz_answer',
            'opencv_image',
            'image',
        )
        labels = {
            'answer_list1': '퀴즈 1문항',
            'answer_list2': '퀴즈 2문항',
            'answer_list3': '퀴즈 3문항',
            'answer_list4': '퀴즈 4문항',
            'quiz_answer': '퀴즈 정답',
            'opencv_image': '변형 이미지',
            'image': '원본 이미지',
        }
