import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import View

from django.shortcuts import render, redirect

from room.models import Room
from .forms import QuizForm
from .models import Quiz

logger = logging.getLogger(__name__)


def check_round(request, room_id):
    room = Room.objects.get(id=room_id)
    room_round = room.play_round
    quiz_count = Quiz.objects.filter(room_id=room_id, user_id=request.user)
    return room_round, quiz_count


class QuizView(LoginRequiredMixin, View):
    """Quiz View"""

    login_url = '/'

    def get(self, request, room_id):
        room = Room.objects.get(id=room_id)
        if request.user.id != room.host_id:
            return redirect('room:assets')
        form = QuizForm()

        room_round, quiz_count = check_round(request, room_id)
        context = {'get_count': quiz_count.count() + 1,
                   'room_round': room_round,
                   'form': form,
                   'room_id': room_id}

        return render(request, 'quiz/create_quiz.html', context)

    def post(self, request, room_id):
        form = QuizForm(request.POST)
        room_round, quiz_count = check_round(request, room_id)

        # Exception
        if not form.is_valid() or 'image' not in request.FILES.keys():
            image = 'image' in request.FILES.keys()
            context = {'get_count': quiz_count.count() + 1,
                       'room_round': room_round,
                       'image': image,
                       'room_id': room_id,
                       'form': form}
            return render(request, 'quiz/create_quiz.html', context)

        # Process
        form.save(user=request.user, image_file=request.FILES['image'], room_id=room_id)

        room_round, quiz_count = check_round(request, room_id)

        if quiz_count.count() != room_round:
            return self.get(request, room_id)
        else:
            return redirect('room:asset', room_id=room_id)
