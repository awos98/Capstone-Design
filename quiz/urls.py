from django.urls import path
from . import views

app_name = 'quiz'

urlpatterns = [
    path('create/', views.QuizView.as_view(), name='create'),
]
