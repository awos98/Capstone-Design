import os

from django.conf import settings
from django.urls import reverse

from quiz.models import Quiz
from room.models import Room
from tests.base import FileTestCase


class QuizBaseTestCase(FileTestCase):
    @classmethod
    def _count_original_images(cls):
        return len(os.listdir(os.path.join(settings.MEDIA_ROOT, settings.ORIGINAL_IMAGE_FILE_PATH)))

    @classmethod
    def _count_opencv_images(cls):
        return len(os.listdir(os.path.join(settings.MEDIA_ROOT, settings.OPENCV_IMAGE_FILE_PATH)))


class QuizModelTestCase(QuizBaseTestCase):
    def test_퀴즈_엔티티를_제거하면_파일도_같이_제거된다(self):

        # 1. Given - 특정 상황에서
        Quiz.objects.create(
            answer_list1="answer1",
            answer_list2="answer2",
            answer_list3="answer3",
            answer_list4="answer4",
            quiz_answer=3,
            quiz_answer_name="answer3",
            opencv_image=self.get_image_file(),
            image=self.get_image_file(),
        )

        expected_original_image_count = self._count_original_images() - 1
        expected_opencv_image_count = self._count_opencv_images() - 1

        # 2. When - 어떤 작업을 수행하면
        Quiz.objects.first().delete()

        # 3. Then - 이런 결과가 나와야 된다.
        self.assertEqual(expected_original_image_count, self._count_original_images())
        self.assertEqual(expected_opencv_image_count, self._count_opencv_images())


class QuizViewTestCase(QuizBaseTestCase):

    def setUp(self):
        self.clean_up_image_file()
        self.login()
        self._room = Room.objects.create(
            name='test1',
            play_round=5,
            limit_time=10,
            host=self.request.user,
        )

    def test_비로그인_상태에서_퀴즈_뷰_요청_시_로그인_페이지로_이동된다(self):
        self.logout()
        response = self.client.get(reverse('room:quiz:create', kwargs={'room_id': self._room.id}))

        self.assertEqual(302, response.status_code)
        self.assertEqual(f"{reverse('signup')}?next={reverse('room:quiz:create', kwargs={'room_id': self._room.id})}",
                         response.url)

    # def test_로그인_상태에서_퀴즈_뷰_요청_시_200_응답이_반환된다(self):
    #     self.login()
    #
    #     response = self.client.get(reverse('room:quiz:create', kwargs={'room_id': self._room.id}))
    #
    #     self.assertEqual(200, response.status_code)

    # def test_로그인_상태에서_모든_정보를_제공하여_퀴즈_생성_시_정상적으로_생성된다(self):
    #     self.login()
    #
    #     self.assertEqual(0, Quiz.objects.count())
    #
    #     expected_original_image_count = self._count_original_images() + 1
    #     expected_opencv_image_count = self._count_opencv_images() + 1
    #
    #     print('test_quiz 생성중')
    #     response = self.client.post(reverse('room:quiz:create', kwargs={'room_id': self._room.id}), {
    #         'answer_list1': 'answer1',
    #         'answer_list2': 'answer2',
    #         'answer_list3': 'answer3',
    #         'answer_list4': 'answer4',
    #         'quiz_answer': 3,
    #         'image': self.get_image_file(),
    #     })
    #
    #     self.assertEqual(302, response.status_code)
    #     self.assertEqual(1, Quiz.objects.count())
    #
    #     self.assertEqual(expected_original_image_count, self._count_original_images())
    #     self.assertEqual(expected_opencv_image_count, self._count_opencv_images())

    def test_로그인_상태에서_일부_정보를_누락하여_퀴즈_생성_시_퀴즈가_생성되지_않는다(self):
        self.login()

        self.assertEqual(0, Quiz.objects.count())

        expected_original_image_count = self._count_original_images()
        expected_opencv_image_count = self._count_opencv_images()

        response = self.client.post(reverse('room:quiz:create', kwargs={'room_id': self._room.id}), {
            'answer_list1': 'answer1',
            'answer_list2': 'answer2',
            'answer_list3': 'answer3',
            'answer_list4': 'answer4',
            'quiz_answer': 3,
        })

        self.assertEqual(200, response.status_code)
        self.assertEqual(0, Quiz.objects.count())

        self.assertEqual(expected_original_image_count, self._count_original_images())
        self.assertEqual(expected_opencv_image_count, self._count_opencv_images())
