import os

from django.conf import settings
from django.db import models
from django.db.models.signals import post_delete
from django.dispatch import receiver

from common.models import User
from room.models import Room


class Quiz(models.Model):
    answer_list1 = models.CharField(max_length=25, null=True)
    answer_list2 = models.CharField(max_length=25, null=True)
    answer_list3 = models.CharField(max_length=25, null=True)
    answer_list4 = models.CharField(max_length=25, null=True)

    quiz_answer = models.IntegerField(default=0)
    quiz_answer_name = models.CharField(max_length=25, null=True)

    # 추가된 opencv img 칼럼
    opencv_image = models.ImageField(upload_to=os.path.join(settings.OPENCV_IMAGE_FILE_PATH),
                                     blank=True, null=True)
    image = models.ImageField(upload_to=os.path.join(settings.ORIGINAL_IMAGE_FILE_PATH),
                              blank=True, null=True)

    # room_id, user_id FK(외래키) 생성
    room = models.ForeignKey(Room, on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)

    def __int__(self):
        return self.quiz_answer


@receiver(post_delete, sender=Quiz)
def after_delete_quiz_files(sender, instance, *args, **kwargs):
    """DB 삭제시 실제 파일도 제거"""
    try:
        instance.opencv_image.delete(save=False)
        instance.image.delete(save=False)
    except:
        pass
