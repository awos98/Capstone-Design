from django.urls import path
from . import views

app_name = 'play'

urlpatterns = [
    path('round/', views.play_round, name='round'),
    path('after/', views.play_round_after, name='after'),
    path('delete/<int:quiz_id>', views.delete, name='delete'),
    path('result/', views.play_result, name='result'),
    path('prev/<int:user_id>', views.prev_page_redirect, name='prev'),
]
