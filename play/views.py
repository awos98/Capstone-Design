import logging
import time

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.contrib import messages

from room.models import Room
from quiz.models import Quiz
from common.models import User

# 대부분의 logger 는 위치를 알기 위한 정보였으므로 판단하에 삭제해도 무방
logger = logging.getLogger(__name__)


@login_required(login_url='/')
def play_round(request, room_id):
    room = Room.objects.get(id=room_id)
    user = User.objects.get(id=request.user.id)
    quiz = Quiz.objects.filter(user_id=room.host.id, room_id=room_id)

    play_round = room.play_round
    limit_time = room.limit_time
    logger.debug(f'play_round')

    quiz_first = quiz.first()

    round_num = play_round - quiz.count() + 1
    user.refresh_check += 1
    user.save()

    # 새로고침의 경우
    if ((round_num + (round_num - 1)) - user.refresh_check) != 0:
        quiz = Quiz.objects.filter(user_id=room.host.id, room_id=room_id)
        if_user_host(user, room_id)
        time.sleep(0.5)
        user.delete()
        return redirect('signup')

    context = {'quiz': quiz_first,
               'limit_time': limit_time,
               'round_num': round_num,
               'play_round': play_round,
               'room_id': room_id}

    return render(request, 'play/play_round.html', context)


@login_required(login_url='/')
def play_round_after(request, room_id):
    room = Room.objects.get(id=room_id)
    user = User.objects.get(id=request.user.id)
    quiz = Quiz.objects.filter(user_id=room.host.id, room_id=room_id)

    # 퀴즈가 없고 방이 시작되어 있는 경우 - host가 바뀐 경우 모두 만족함. 다만 늦게 온 user는 81라인을 거침
    if not quiz.exists() and room.is_start == 1:
        messages.warning(request, "호스트가 방에서 나가 게임이 중단되었습니다.")
        user.ready = 0
        user.refresh_check = 0
        user.save()
        room.is_start = 0
        room.save()

        return redirect('room:asset', room_id=room_id)

    play_round = room.play_round
    round_num = play_round - quiz.count() + 1
    user.refresh_check += 1
    user.save()

    # 비정상적인 접근과 host가 나감에 따른 접근을 구분하는 코드
    if room.is_start == 0:
        if user.refresh_check > 1 and not quiz.exists():
            return redirect('room:play:prev', room_id=room_id, user_id=user.id)
        else:
            return redirect('room:assets')
    if room.is_start == 1 and user not in room.users.all():
        return redirect('room:assets')

    # 새로 고침의 경우 host 일때와 일반 유저의 경우 - host 는 websocket 으로 보냄
    time.sleep(0.5)
    if ((round_num * 2) - user.refresh_check) != 0:
        if room.host_id == user.id:
            if_user_host(user, room_id)
            time.sleep(0.5)
            room = Room.objects.get(id=room_id)
            host_name = room.host
            print(room.host)
            channel_layer = get_channel_layer()
            async_to_sync(channel_layer.group_send)(
                'play_%s' % room_id,
                {
                    'type': 'websocket_host_exited',
                    'host': str(host_name),
                    'command': 'host_exited'
                }
            )
        user.delete()
        return redirect('room:assets')

    user_answer = request.POST['U_ans']
    logger.debug(f'after')
    quiz = Quiz.objects.filter(user_id=room.host.id, room_id=room_id).first()
    if quiz.quiz_answer == int(user_answer):
        user.score += 10
        user.save()

    context = {'quiz': quiz,
               'U_ans': int(user_answer),
               'player': user,
               'room': room,
               'room_id': room_id}

    return render(request, 'play/play_round_after.html', context)


def delete(request, room_id, quiz_id):
    room = Room.objects.get(id=room_id)
    quiz = Quiz.objects.filter(user_id=room.host.id, room_id=room_id)

    if request.user.id == room.host.id:
        Quiz.objects.get(id=quiz_id).delete()

    # 지연시간 후 작동 quiz 있을 시 round 없을 시 아래 코드로 가서 result
    time.sleep(1)
    if quiz.exists():
        room.play_user_count = 0
        room.save()
        return redirect('room:play:round', room_id=room_id)

    user = User.objects.get(id=request.user.id)
    user.is_staff = True
    user.save()
    room.play_user_count = 0
    room.is_start = 0
    room.save()
    return redirect('room:play:result', room_id=room_id)


@login_required(login_url='/')
def play_result(request, room_id):
    room = Room.objects.get(id=room_id)
    user = User.objects.get(id=request.user.id)

    ranks, sorted_users_score = users_score(room)

    users_scores = [list(scores) for scores in sorted_users_score]

    key = [users_scores[i][0] for i in range(len(users_scores))]
    value = [users_scores[i][1] for i in range(len(users_scores))]

    ranks_scores = zip(ranks, key, value)

    if room.host_id == user.id:
        rank = 0
    else:
        rank = ranks[key.index(user.username)]
    room.is_start = 0
    room.save()
    user.ready = 0
    user.refresh_check = 0
    user.save()

    if not user.is_staff or user not in room.users.all():
        return redirect('room:assets')

    user.is_staff = False
    user.save()

    context = {
        'room_id': room_id,
        'user_id': user.id,
        'username': user.username,
        'host': room.host_id,
        'ranks_scores': ranks_scores,
        'ranking': rank,
    }

    return render(request, 'play/play_result.html', context)


def users_score(room):
    room_users = room.users.all()
    users_score = {}
    for i in range(len(room_users)):
        if room_users[i].id != room.host_id:
            users_score[room_users[i].username] = room_users[i].score

    sorted_users_scores = sorted(users_score.items(), reverse=True, key=lambda item: item[1])
    sorted_users_ranks = [sorted_users_scores[i][1] for i in range(len(sorted_users_scores))]

    ranks = []
    for i in range(0, len(sorted_users_ranks)):
        r = 1
        for j in range(0, len(sorted_users_ranks)):
            if sorted_users_ranks[i] < sorted_users_ranks[j]:
                r += 1
        ranks.append(r)

    return ranks, sorted_users_scores


def if_user_host(user, room_id):
    room = Room.objects.get(id=room_id)
    room.users.remove(user)

    # 방에 user 있을 시 방장을 넘기고 없을 시 방이 사라짐
    if user.id == room.host_id:
        if not room.users.exists():
            room.delete()
        else:
            room.host_id = None
            room.host_id = room.users.all().order_by('?').first()
            room.save()


# 이전 페이지로 가는 로직(after에서 이전으로 버튼), round 및 after 함수에서도 redirect 중
def prev_page_redirect(request, room_id, user_id):
    messages.warning(request, "호스트가 방에서 나가 게임이 중단되었습니다.")
    room = Room.objects.get(id=room_id)
    user = User.objects.get(id=user_id)
    user.ready = 0
    user.refresh_check = 0
    user.save()
    room.is_start = 0
    room.save()

    return redirect('room:asset', room_id=room_id)
