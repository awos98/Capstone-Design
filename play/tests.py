from django.test import TestCase

# Create your tests here.
from quiz.models import Quiz
from quiz.tests import QuizBaseTestCase
from room.models import Room
from django.urls import reverse


class PlayModelTests(QuizBaseTestCase):
    def test_방_생성후_일치확인(self):
        self.login()
        Room.objects.create(
            name='test',
            play_round=5,
            limit_time=10,
            host=self.request.user,
        )

        room = Room.objects.get(id=1)
        self.assertEqual(room.name, 'test')

    def test_퀴즈_삭제_확인(self):
        Quiz.objects.create(
            answer_list1="answer1",
            answer_list2="answer2",
            answer_list3="answer3",
            answer_list4="answer4",
            quiz_answer=1,
            quiz_answer_name="answer1",
            opencv_image=self.get_image_file(),
            image=self.get_image_file(),
        )
        Quiz.objects.create(
            answer_list1="answer1",
            answer_list2="answer2",
            answer_list3="answer3",
            answer_list4="answer4",
            quiz_answer=2,
            quiz_answer_name="answer2",
            opencv_image=self.get_image_file(),
            image=self.get_image_file(),
        )

        quiz = Quiz.objects.all()
        self.assertEqual(quiz.count(), 2)

        Quiz.objects.first().delete()
        self.assertEqual(quiz.count(), 1)
        self.assertEqual(quiz[0].id, 2)


class PlayViewTestCase(QuizBaseTestCase):

    def setUp(self):
        self._quiz = Quiz.objects.create(
            answer_list1="answer1",
            answer_list2="answer2",
            answer_list3="answer3",
            answer_list4="answer4",
            quiz_answer=1,
            quiz_answer_name="answer1",
            opencv_image=self.get_image_file(),
            image=self.get_image_file(),
        )

    def test_delete로직_redirect_확인코드(self):
        self.login()
        self._room = Room.objects.create(
            name='test1',
            play_round=5,
            limit_time=10,
            host=self.request.user,
        )

        context = {'room_id': self._room.id,
                   'quiz_id': self._quiz.id}
        response = self.client.post(reverse('room:play:delete', kwargs=context))

        # redirect 는 302 코드가 나옴
        self.assertEqual(302, response.status_code)

        quiz = Quiz.objects.all()
        self.assertEqual(quiz.count(), 0)

    # def test_비로그인_상태에서_playdelete_요청_시_로그인_페이지로_이동된다(self):
    #     self.login()
    #     self._room = Room.objects.create(
    #         name='test1',
    #         play_round=5,
    #         limit_time=10,
    #         host=self.request.user,
    #     )
    #     self.logout()
    #     context = {'room_id': self._room.id,
    #                'quiz_id': self._quiz.id}
    #
    #     response = self.client.get(reverse('room:play:delete', kwargs=context))
    #
    #     self.assertEqual(302, response.status_code)
    #     self.assertEqual(f"{reverse('signup')}?next={reverse('room:play:delete', kwargs=context)}",
    #                      response.url)
