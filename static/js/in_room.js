const roomId = document.getElementById('room-name').getAttribute('value');
const username = document.querySelector('#room-name').getAttribute('user');
const host = document.querySelector('#room-name').getAttribute('host');

let button = document.getElementById('ready');

// 클라이언트 쪽에서 서버와 통신할 웹소켓을 만들고
// ws 프로토콜 사용(wss와 ws는 https와 http와의 관계와 유사)
let ws;
if (window.location.protocol === 'http:') {
    //http
    ws = new WebSocket(
        'ws://'
        + window.location.host
        + '/ws/chat/'
        + roomId
        + '/'
    );
} else {
    //https
    ws = new WebSocket(
        'wss://'
        + window.location.host
        + '/ws/chat/'
        + roomId
        + '/'
    );
}

//방 입장 시 유저의 정보를 서버에 전송
ws.onopen = function (e) {
    ws.send(
        JSON.stringify({
            command: "joined",
            host: host,
        }));
}

// 서버로부터 데이터 수신
ws.onmessage = function (e) {
    const data = JSON.parse(e.data);
    const command = data.command;
    if (command === "chat") {
        document.querySelector('#chat-log').value += (data.username + ' : ');
        document.querySelector('#chat-log').value += (data.message + '\n');
        document.getElementById("chat-log").scrollTop = document.getElementById("chat-log").scrollHeight;
    }

    if (command === "joined") {
        isUserReady(data);
        isHostSameUsername(data);

    }

    if (command === "exited") {
        isUserReady(data);
        isHostSameUsername(data);
    }

    if (command === "ready") {
        isUserReady(data);
        isHostSameUsername(data);
    }

    if (command === "unready") {
        isUserReady(data);
        isHostSameUsername(data);
    }

    if (command === "start") {
        document.getElementById('start-button').click();
    }

    if (command === "create") {
        document.querySelector('#chat-log').value += ('방장이 퀴즈를 만들고 있습니다.\n');
    }

};

// 연결 시도 중 에러가 발생하면, 먼저 'error'라는 이름의 이벤트가 WebSocket오브젝트로 전달
// 그로 인해 onerror 핸들러 실행
// 그 후에 연결이 종료되는 이유를 가리키는 closeevent가 WebSocket오브젝트로 전달되고
// 그로 인해 onclose가 실행
document.querySelector('#chat-close').onclick = function (e) {
    ws.send(JSON.stringify({
        command: "exited",
        'exit_user': username,
    }));
    ws.close();
}

ws.onclose = function (e) {
    console.error('Chat socket closed unexpectedly');
};

document.querySelector('#chat-message-input').focus();
document.querySelector('#chat-message-input').onkeyup = function (e) {
    if (e.keyCode === 13) {  // enter, return
        document.querySelector('#chat-message-submit').click();
    }
};

// 서버에 데이터를 전송
document.querySelector('#chat-message-submit').onclick = function (e) {
    const messageInputDom = document.querySelector('#chat-message-input');
    const message = messageInputDom.value;
    if (message !== '') {
        ws.send(JSON.stringify({
            'message': message,
            'username': username,
            command: "chat",
        }));
    }
    messageInputDom.value = '';
    //줄내림
    document.getElementById("chat-log").scrollTop = document.getElementById("chat-log").scrollHeight;
};

// host 일 경우 play-submit 아닐 경우 ready
if (username === host) {
    const create = document.querySelector('#create-button').onclick = function (e) {
        ws.send(JSON.stringify({
            command: "create",
        }));
        ws.close();
    }
} else {
    button.addEventListener('click', Ready);

    function Ready() {
        button.removeEventListener('click', Ready);
        button.addEventListener('click', Unready);
        button.value = 'unready';
        ws.send(JSON.stringify({
            'username': username,
            command: "unready",
        }));
    }

    function Unready() {
        button.removeEventListener('click', Unready);
        button.addEventListener('click', Ready);
        button.value = 'ready'
        ws.send(JSON.stringify({
            'username': username,
            command: "ready"
        }));
    }
}

const start = document.querySelector('#start-button').onclick = function (e) {
    ws.send(JSON.stringify({
        command: "start",
    }));
    ws.close();
}

const create = document.querySelector('#create-button').onclick = function (e) {
    ws.send(JSON.stringify({
        command: "create",
    }));
    ws.close();
}

function isHostSameUsername(data) {
    if (data.host === username) {
        document.getElementById('create-button').hidden = false;
        document.getElementById('start-button').hidden = false;
        document.getElementById('ready').hidden = true;
        if (data.status === false) {
            document.querySelector("#start-button").setAttribute('class', 'btn btn-success disabled');
        } else {
            document.querySelector("#start-button").setAttribute('class', 'btn btn-success');
        }
    } else {
        document.getElementById('ready').hidden = false;
    }
}

function isUserReady(data) {
    let user;
    let status;

    $('#user_list').empty();
    for (let i = 0; i < data.user.length; i++) {
        if (data.host === data.user[i]) {
            user = data.user[i];
            status = "host";
            userTable(user, status)
        } else {
            if (data.user_ready_list[i] === 1) {
                user = data.user[i];
                status = "O"
                userTable(user, status)
            } else {
                user = data.user[i];
                status = "X"
                userTable(user, status)
            }
        }
    }
}


function userTable(user, status) {
    let userTable = document.getElementById("user_list");
    let mine = '<tr class="bg-success text-white">' +
        '<td>' + user + '</td>' +
        '<td>' + status + '</td>' +
        '</tr>'
    let others = '<tr class="bg-secondarytext-white">' +
        '<td>' + user + '</td>' +
        '<td>' + status + '</td>' +
        '</tr>'

    if (user === username) userTable.innerHTML += mine;
    else userTable.innerHTML += others;
}
