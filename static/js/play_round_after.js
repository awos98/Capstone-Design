const roomId = document.getElementById('room-name').getAttribute('value');
const username = document.querySelector('#room-name').getAttribute('user');
const host = document.querySelector('#room-name').getAttribute('host');

// 클라이언트 쪽에서 서버와 통신할 웹소켓을 만들고
// ws 프로토콜 사용(wss와 ws는 https와 http와의 관계와 유사)
let ws;
if (window.location.protocol === 'http:') {
    //http
    ws = new WebSocket(
        'ws://'
        + window.location.host
        + '/ws/play/'
        + roomId
        + '/'
    );
} else {
    //https
    ws = new WebSocket(
        'wss://'
        + window.location.host
        + '/ws/play/'
        + roomId
        + '/'
    );
}

ws.onopen = function () {
    ws.send(
        JSON.stringify({
            command: "joined",
            host: host,
        }));
}

ws.onmessage = function (e) {
    const data = JSON.parse(e.data);
    const command = data.command;

    if (command === "joined") {
        // console.log(data.user);
        // console.log(data.host);
        // console.log("data.users_score: " + data.users_score);
        // console.log("data.users_rank: " + data.users_rank);
        console.log(data.status);

        if (data.status == true) {
            if (data.host)
                document.getElementById("next-round-button").setAttribute('class', 'btn btn-success');
            scoreTable(data);
        } else {
            isUpdateScore();

        }
        if (data.host === username) {
            document.getElementById('next-round-button').hidden = false;
        }
    }

    if (command === 'next') {
        document.getElementById('next-round-button').click();
    }

    if (command === 'host_exited') {
        if (data.host === username) {
            document.getElementById('prev-page-room').hidden = false;
            document.getElementById('next-round-button').hidden = true;
        }
        alert('호스트가 바뀌었습니다.')
    }

    if (command === 'back') {
        document.getElementById('prev-page-room').click();
        document.querySelector("#prev-page-room").setAttribute('class', 'btn btn-danger disabled');
    }


}

document.querySelector('#next-round-button').onclick = function (e) {
    document.querySelector("#next-round-button").setAttribute('class', 'btn btn-success disabled');
    ws.send(JSON.stringify({
        command: "next",
    }));
    document.getElementById('post-form').submit();
    ws.close();
}

document.querySelector('#prev-page-room').onclick = function (e) {
    ws.send(JSON.stringify({
        command: "back",
    }));
    ws.close();
}

//새로고침 방지 코드
function doNotReload() {
    if ((Event.ctrlKey == true && (Event.keyCode == 78 || Event.keyCode == 82)) //ctrl+N , ctrl+R
        || (Event.keyCode == 116)) // function F5
    {
        Event.keyCode = 0;
        Event.cancelBubble = true;
        Event.returnValue = false;
    }
}

document.onkeydown = doNotReload;

function noEvent() {
    if (Event.keyCode == 116) {
        Event.keyCode = 2;
        return false;
    } else if (Event.ctrlKey && (Event.keyCode == 78 || Event.keyCode == 82)) {
        return false;
    }
}

document.onkeydown = noEvent;

function scoreTable(data) {
    let table = document.getElementById("user_list");
    $('#user_list').empty();
    for (let i = 0; i < data.users_score.length; i++) {
        let mine = '<tr class="bg-success text-white">' +
            '<td>' + (data.users_rank[i]) + '</td>' +
            '<td>' + data.users_score[i][0] + '</td>' +
            '<td>' + data.users_score[i][1] + '</td>' +
            '</tr>'
        let others = '<tr class="bg-secondarytext-white">' +
            '<td>' + (data.users_rank[i]) + '</td>' +
            '<td>' + data.users_score[i][0] + '</td>' +
            '<td>' + data.users_score[i][1] + '</td>' +
            '</tr>'
        if (data.users_score[i][0] === username) {
            table.innerHTML += mine
        } else {
            table.innerHTML += others
        }
    }
}

function isUpdateScore() {
    let update = document.getElementById("user_list");
    let description = '<tr class="bg-secondarytext-white">' +
        '<td colspan="3">집계될 때까지 잠시만 기다려주세요.</td>' +
        '</tr>'
    update.innerHTML += description
}