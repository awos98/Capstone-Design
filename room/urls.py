from django.urls import path, include
from . import views

app_name = 'room'

urlpatterns = [
    path('', views.index, name="assets"),
    path('create_room/', views.create_room, name='create'),
    path('<int:room_id>/', views.room, name='asset'),
    path('<int:room_id>/quizs/', include('quiz.urls')),
    path('<int:room_id>/plays/', include('play.urls')),
]
