# import os
#
# from django.conf import settings
# from django.shortcuts import redirect, get_object_or_404
# from django.template.context_processors import request
# from django.urls import reverse
#
# from room.models import Room
# from common.models import User
# from tests.base import FileTestCase, BaseTestCase
#
#
# class RoomViewTestCase(FileTestCase):
#     def setUp(self):
#         self.login()
#         self._room = Room.objects.create(
#             name='test1',
#             play_round=5,
#             limit_time=10,
#             host=self.request.user,
#         )
#
#     def test_비로그인_상태에서_room_delete_이용시_로그인_페이지_이동(self):
#         self.logout()
#         context = {'room_id': self._room.id}
#         response = self.client.get(reverse('room:delete', kwargs={'room_id': self._room.id}))
#
#         self.assertEqual(302, response.status_code)
#
#     def test_host가_아무도_없는_방에_나갔을_시_room_삭제(self):
#         context = {'room_id': self._room.id}
#         self.client.get(reverse('room:delete', kwargs=context))
#
#         response = self.client.get(reverse('room:asset', kwargs=context))
#
#         self.assertEqual(404, response.status_code)
#
#     def test_host가_방에서_나갔을_시_다른_유저에게_host(self):
#         user2 = User.objects.create_user(username='user2', password='asd')
#         self.assertEqual(User.objects.all().count(), 2)
#         print(self._room.host)
#
#         room = Room.objects.get(id=self._room.id)
#         user = User.objects.get(id=user2.id)
#
#         room.users.add(user)
#
#         print(self._room.users.all())
#         context = {'room_id': self._room.id}
#         self.client.get(reverse('room:delete', kwargs=context))
#         room2 = Room.objects.get(id=self._room.id)
#         print(room2.host)
#
