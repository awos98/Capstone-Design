from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from common.models import User

class Room(models.Model):
    name = models.CharField(max_length=20)

    play_round = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(10)])
    limit_time = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(30)])
    host = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True, related_name='host_test')
    users = models.ManyToManyField(User, related_name='users_test_name')
    is_start = models.IntegerField(default=0)
    play_user_count = models.IntegerField(default=0)

    def __str__(self):
        return self.name
