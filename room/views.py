import logging

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404

from quiz.models import Quiz
from .forms import RoomForm
from .models import Room
from common.models import User

logger = logging.getLogger(__name__)


def index(request):
    if not request.user.is_authenticated:
        return redirect('signup')

    user = User.objects.get(id=request.user.id)
    user.ready = 0
    user.save()

    room_all = Room.objects.all()
    for room_data in room_all:
        if user in room_data.users.all():
            room_data.users.remove(user)
        if user.id == room_data.host_id:
            if not room_data.users.exists():
                room_data.delete()
            else:
                room_data.host_id = None
                room_data.host_id = room_data.users.all().order_by('?').first()
                room_data.save()

    room_list = Room.objects.all()
    context = {'room_list': room_list}
    return render(request, 'room/room_list.html', context)


@login_required(login_url='/')
def create_room(request):
    if not request.user.is_authenticated:
        return redirect('signup')
    logger.debug(request.user)
    if request.method == "POST":
        form = RoomForm(request.POST)
        # logger.debug()
        if form.is_valid():
            room = form.save(commit=False)
            room.host = request.user
            room.save()
            return redirect('room:asset', room_id=room.id)
    else:
        form = RoomForm()
    return render(request, 'room/create_room.html', {'form': form,
                                                     'number': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]})


@login_required(login_url='/')
def room(request, room_id):
    if not request.user.is_authenticated:
        return redirect('signup')

    room = get_object_or_404(Room, pk=room_id)
    print(f'room type : {room, type(room), room.id}')
    room_all = Room.objects.all()
    user = User.objects.get(id=request.user.id)
    for room_data in room_all:
        if user in room_data.users.all() and user not in room.users.all():
            return redirect('room:assets')

    if room.is_start == 1:
        return redirect('room:assets')

    player = str(request.user)

    room.users.add(request.user)
    room.save()

    # 임의적으로 user 의 score 를 0 으로 변환하는 코드
    user = request.user
    user.score = 0
    user.refresh_check = 0
    user.save()

    quiz_count = Quiz.objects.filter(room_id=room_id, user_id=request.user)

    context = {'room': room,
               'room_round': room.play_round,
               'quiz_count': quiz_count.count(),
               'username': player}
    return render(request, 'room/in_room.html', context)
