from django import forms
from .models import Room


class RoomForm(forms.ModelForm):
    class Meta:
        model = Room
        fields = ('name', 'play_round', 'limit_time')
        labels = {
            'name': '방제목',
            'play_round': '라운드',
            'limit_time': '제한시간',
        }
