import cv2
import numpy as np
import random


def canny_import(image_file):
    encoded_image = np.fromstring(image_file, dtype=np.uint8)
    gray_img = cv2.imdecode(encoded_image, cv2.IMREAD_GRAYSCALE)

    threshold1 = 200
    threshold2 = 200
    edge_img = cv2.Canny(gray_img, threshold1, threshold2)

    return edge_img


def findContours_import(image_file):
    encoded_image = np.fromstring(image_file, dtype=np.uint8)
    img = cv2.imdecode(encoded_image, cv2.IMREAD_COLOR)
    h, w, c = img.shape

    # 검정 배경
    # black = np.zeros((h, w, c), np.uint8)
    white = np.zeros((h, w, c), np.uint8)
    white.fill(255)

    # color -> gray
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    ret, binary = cv2.threshold(gray, 210, 255, cv2.THRESH_BINARY)
    binary = cv2.bitwise_not(binary)
    # 이진화

    # RETR_EXTERNAL 최와곽 라인만 찾음
    contours, hierachy = cv2.findContours(binary, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    # 외곽선 그리기
    # 검은 배경
    # cv2.drawContours(black, contours, -1, (0, 0, 255), 2)
    # 하얀 배경
    cv2.drawContours(white, contours, -1, (0, 0, 255), 2)

    return white


def mosaic_import(image_file):
    encoded_image = np.fromstring(image_file, dtype=np.uint8)

    img = cv2.imdecode(encoded_image, cv2.IMREAD_COLOR)  # 이미지 읽기
    result_img = mosaic(img, ratio=0.05)

    return result_img


def mosaic(image, ratio=0.1):
    small = cv2.resize(image, None, fx=ratio, fy=ratio, interpolation=cv2.INTER_NEAREST)
    return cv2.resize(small, image.shape[:2][::-1], interpolation=cv2.INTER_NEAREST)


def blur_import(image_file):
    encoded_image = np.fromstring(image_file, dtype=np.uint8)
    img = cv2.imdecode(encoded_image, cv2.IMREAD_COLOR)
    ksize = 150  # 블러 처리에 사용할 커널 크기

    roi = cv2.blur(img, (ksize, ksize))  # 블러(모자이크) 처리

    return roi


def random_image_import(image_file):
    encoded_image = np.fromstring(image_file, dtype=np.uint8)
    img = cv2.imdecode(encoded_image, cv2.IMREAD_COLOR)

    for i in range(16):
        globals()['org_{0}'.format(i)] = img.copy()
    print(img.shape)
    row, col = img.shape[:2]
    row_list = [row / 4, row * 2 / 4, row * 3 / 4, row]
    col_list = [col / 4, col * 2 / 4, col * 3 / 4, col]
    row_list = list(map(int, row_list))
    col_list = list(map(int, col_list))
    img_list = []
    first_col = 0
    count = 0

    for col in row_list:
        first_row = 0
        for row in col_list:
            img_list.append(globals()['org_{0}'.format(count)][first_col:col, first_row:row])
            first_row = row
        first_col = col
        count += 1

    avg_row = int(row / 4)
    avg_col = int(col / 4)
    resize_img_list = []

    for i in img_list:
        resize_img_list.append(cv2.resize(i, (avg_row, avg_col)))

    random.shuffle(resize_img_list)

    rand_img_list = []
    for i in [0, 4, 8, 12]:
        a = resize_img_list[i + 0]
        b = resize_img_list[i + 1]
        c = resize_img_list[i + 2]
        d = resize_img_list[i + 3]
        rand_img_list.append(cv2.hconcat([a, b, c, d]))

    rand_img_list = cv2.vconcat([rand_img_list[0], rand_img_list[1], rand_img_list[2], rand_img_list[3]])

    return rand_img_list


def lens_import(image_file):
    encoded_image = np.fromstring(image_file, dtype=np.uint8)
    img = cv2.imdecode(encoded_image, cv2.IMREAD_COLOR)
    rows, cols = img.shape[:2]

    exp = 5  # 볼록, 오목 지수 (오목 : 0.1 ~ 1, 볼록 : 1.1~)
    scale = 1  # 변환 영역 크기 (0 ~ 1)

    # 매핑 배열 생성
    mapy, mapx = np.indices((rows, cols), dtype=np.float32)

    # 좌상단 기준좌표에서 -1~1로 정규화된 중심점 기준 좌표로 변경
    mapx = 2 * mapx / (cols - 1) - 1
    mapy = 2 * mapy / (rows - 1) - 1

    # 직교좌표를 극 좌표로 변환
    r, theta = cv2.cartToPolar(mapx, mapy)

    # 왜곡 영역만 중심확대/축소 지수 적용
    r[r < scale] = r[r < scale] ** exp

    # 극 좌표를 직교좌표로 변환
    mapx, mapy = cv2.polarToCart(r, theta)

    # 중심점 기준에서 좌상단 기준으로 변경
    mapx = ((mapx + 1) * cols - 1) / 2
    mapy = ((mapy + 1) * rows - 1) / 2
    # 재매핑 변환
    distorted = cv2.remap(img, mapx, mapy, cv2.INTER_LINEAR)

    ####
    l = 30  # 파장(wave length)
    amp = 30  # 진폭(amplitude)

    mapy, mapx = np.indices((rows, cols), dtype=np.float32)

    # sin, cos 함수를 적용한 변형 매핑 연산 ---②
    sinx = mapx + amp * np.sin(mapy / l)
    cosy = mapy + amp * np.cos(mapx / l)

    # x,y 축 모두 sin, cos 곡선 적용 및 외곽 영역 보정
    img_both = cv2.remap(distorted, sinx, cosy, cv2.INTER_LINEAR, \
                         None, cv2.BORDER_REPLICATE)

    return img_both
