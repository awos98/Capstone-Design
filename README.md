# \[OpenCV를 활용한 웹기반 퀴즈게임\]

## 기획
- 예시) [home](https://gitlab.com/awos98/Capstone-Design/-/wikis/home)

## ※ 목표

- 비밀번호없이 로그인 구현
- 방생성, 방참가 및 퀴즈생성 구현
- 유저들 간 채팅 구현
- 게임플레이 구현

## ※ 역할
| PM | 서버 관리 | Back-End 개발 | Front-End 개발 |
|----|-------|-------------|--------------|
| 최규진(조장) | 윤원석(1팀장) | 최규진(2팀장) | 김도유(3팀장) |
|  |  | 김도유 | 윤원석 |
|  |  | 윤원석 | 최규진 |

- PM(Project Manager): 프로젝트 관리자
- 서버 관리팀: Web, GitLab, DB, 서버 관리
- Back-End 개발팀: Django, DB 관련 개발
- Front-End 개발팀: HTML, CSS, JS 관련 개발

## ※ 개발 전 설정

* [ ] PyCharm 설치
* [ ] Anaconda 설치 및 가상환경 생성
* [ ] GitLab 에서 프로젝트 클론 및 깃 연동
* [ ] MySQL 설치 및 프로젝트와 연동
* [ ] requirements.txt 파일로 패키지 한꺼번에 설치

## 동작 과정
![졸작](/uploads/a4144b015f807a8bdabb1c2cd448177a/졸작.gif)
