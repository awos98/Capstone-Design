from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from django.contrib import messages

from .forms import UsersForm


def signup_login(request):
    if request.user.is_authenticated:
        return redirect('room:assets')

    if request.method == "POST":
        raw_password = '1q2w3e4r!@'
        form = UsersForm(request.POST)

        try:
            if form.is_valid():
                form.save(password=raw_password)
                username = form.cleaned_data.get('username')
                auth = 0
                user = authenticate(username=username, password=raw_password, auth=auth)
                login(request, user)
                return redirect('room:assets')
        except:
            messages.error(request, "한글 자음 모음, 이모티콘은 입력할 수 없습니다.")
            return redirect('/')
    else:
        form = UsersForm()
    return render(request, 'common/signup_login.html', {'form': form})


def remove_account(request):
    if request.user.is_authenticated:
        request.user.delete()

    logout(request)

    return redirect('signup')
