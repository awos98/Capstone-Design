from django.urls import path
from . import views

urlpatterns = [
    path('logout/', views.remove_account, name='logout'),
]
