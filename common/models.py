from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    auth = models.IntegerField(default=0)
    answer = models.IntegerField(default=0, null=True)
    score = models.IntegerField(default=0)
    ready = models.IntegerField(default=0)
    username = models.CharField(max_length=20, unique=True)
    refresh_check = models.IntegerField(default=0)

    def __str__(self):
        return self.username
