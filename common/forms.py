from django.contrib.auth import get_user_model
from django import forms


class UsersForm(forms.ModelForm):
    def save(self, commit=True, **kwargs):
        return get_user_model().objects.create_user(
            username=self.cleaned_data['username'],
            password=kwargs.get('password'),
        )

    class Meta:
        model = get_user_model()
        fields = ('username',)
        labels = {'username': '유저명'}
